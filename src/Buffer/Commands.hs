{-# LANGUAGE OverloadedLists #-}

module Buffer.Commands where

import Control.Exception (bracket)
import Vulkan.Core10 as Core10
import Vulkan.CStruct.Extends (SomeStruct(..))
import Vulkan.Zero (zero)

import Types (VulkanWindow(..))

oneshot :: VulkanWindow -> (Core10.CommandBuffer -> IO ()) -> IO ()
oneshot VulkanWindow{..} action = do
  let commandBufferAllocateInfo :: CommandBufferAllocateInfo
      commandBufferAllocateInfo = zero
        { commandPool        = vwCommandPool
        , level              = COMMAND_BUFFER_LEVEL_PRIMARY
        , commandBufferCount = 1
        }

  Core10.withCommandBuffers vwDevice commandBufferAllocateInfo bracket $ \case
    [buf] -> do
      let
        oneTime :: CommandBufferBeginInfo '[]
        oneTime = zero { flags = COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT }

      Core10.useCommandBuffer buf oneTime $ action buf

      withFence vwDevice zero Nothing bracket $ \fence -> do
        Core10.queueSubmit vwGraphicsQueue
          [ SomeStruct zero
              { commandBuffers =
                  [ Core10.commandBufferHandle buf
                  ]
              }
          ]
          fence
        Core10.waitForFences vwDevice [fence] True maxBound >>= \case
          SUCCESS ->
            pure ()
          err ->
            error $ "copyBuffer failed: " <> show err
    _ ->
      error "assert: exactly the requested buffer was given"
