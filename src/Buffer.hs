{-# LANGUAGE OverloadedLists #-}

module Buffer where

import Control.Exception (bracket)
import Data.Bits ((.|.))
import Data.Word (Word32)
import Vulkan.NamedType ((:::))
import Vulkan.Zero (zero)

import qualified Foreign
import qualified Vulkan.Core10 as Core10
import qualified VulkanMemoryAllocator as VMA

import Buffer.Commands (oneshot)
import Types (UBO(..), VulkanWindow(..), Vertex(..), vertexItems)

-- * Vertex buffer

withVertices
  :: VulkanWindow
  -> [Vertex]
  -> (Core10.Buffer -> IO c)
  -> IO c
withVertices vw vertices proc = bracket create destroy (proc . fst)
  where
    create = createBuffer vw Core10.BUFFER_USAGE_VERTEX_BUFFER_BIT items

    (items, _itemsSize) = vertexItems @Int vertices

    destroy (buf, alloc) = VMA.destroyBuffer (vwAllocator vw) buf alloc

-- * Index buffer

withIndices :: VulkanWindow -> [Word32] -> (Core10.Buffer -> IO c) -> IO c
withIndices vw indices proc = bracket create destroy (proc . fst)
  where
    create = createBuffer vw Core10.BUFFER_USAGE_INDEX_BUFFER_BIT indices

    destroy (buf, alloc) = VMA.destroyBuffer (vwAllocator vw) buf alloc

createBuffer
  :: Foreign.Storable a
  => VulkanWindow
  -> Core10.BufferUsageFlagBits
  -> [a]
  -> IO (Core10.Buffer, VMA.Allocation)
createBuffer vw usage values = do
  (buf, allocation, _info) <- VMA.createBuffer vwAllocator indexBufferCI indexAllocationCI

  VMA.withBuffer vwAllocator stageBufferCI stageAllocationCI bracket $ \(staging, stage, _stageInfo) -> do
    VMA.withMappedMemory vwAllocator stage bracket $ \ptr -> do
      Foreign.pokeArray (Foreign.castPtr ptr) values
      VMA.flushAllocation vwAllocator stage 0 Core10.WHOLE_SIZE

    copyBuffer vw staging buf itemsSize

  pure (buf, allocation)
  where
    VulkanWindow{..} = vw

    itemsSize = fromIntegral $ Foreign.sizeOf (head values) * length values

    stageBufferCI :: Core10.BufferCreateInfo '[]
    stageBufferCI = zero
      { Core10.size        = itemsSize
      , Core10.usage       = Core10.BUFFER_USAGE_TRANSFER_SRC_BIT
      , Core10.sharingMode = Core10.SHARING_MODE_EXCLUSIVE
      }

    stageAllocationCI :: VMA.AllocationCreateInfo
    stageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Core10.MEMORY_PROPERTY_HOST_VISIBLE_BIT
      }

    indexBufferCI :: Core10.BufferCreateInfo '[]
    indexBufferCI = zero
      { Core10.size        = itemsSize
      , Core10.usage       = Core10.BUFFER_USAGE_TRANSFER_DST_BIT .|. usage
      , Core10.sharingMode = Core10.SHARING_MODE_EXCLUSIVE
      }

    indexAllocationCI :: VMA.AllocationCreateInfo
    indexAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Core10.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

-- * Uniform buffer

withUniform :: Foreign.Storable a => VulkanWindow -> a -> (UBO a -> IO c) -> IO c
withUniform vw initial proc = bracket create destroy proc
  where
    create =
      createUniform vw initial

    destroy UBO{..} =
      VMA.destroyBuffer (vwAllocator vw) uboBuffer uboAllocation

createUniform :: Foreign.Storable a => VulkanWindow -> a -> IO (UBO a)
createUniform vw@VulkanWindow{..} initial = do
  (buf, allocation, _info) <- VMA.createBuffer vwAllocator uboCI uboAllocate

  let
    ubo = UBO
      { uboBuffer     = buf
      , uboAllocation = allocation
      -- , uboUpdate = mkUpdate buf allocation
      }

  updateUniform vw ubo initial

  pure ubo

  where
    uboSize = fromIntegral $ Foreign.sizeOf initial

    uboCI :: Core10.BufferCreateInfo '[]
    uboCI = zero
      { Core10.size        = uboSize
      , Core10.usage       = Core10.BUFFER_USAGE_UNIFORM_BUFFER_BIT
      , Core10.sharingMode = Core10.SHARING_MODE_EXCLUSIVE
      }

    uboAllocate :: VMA.AllocationCreateInfo
    uboAllocate = zero
      { VMA.usage =
          VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags =
          Core10.MEMORY_PROPERTY_HOST_VISIBLE_BIT .|.
          Core10.MEMORY_PROPERTY_HOST_COHERENT_BIT
      }

updateUniform :: Foreign.Storable a => VulkanWindow -> UBO a -> a -> IO ()
updateUniform VulkanWindow{vwAllocator} UBO{..} value = do
  -- putStrLn "updating UBO"
  VMA.withMappedMemory vwAllocator uboAllocation bracket $ \ptr ->
    Foreign.poke (Foreign.castPtr ptr) value

-- * Staging buffer utils

copyBuffer
  :: VulkanWindow
  -> ("src" ::: Core10.Buffer)
  -> ("dst" ::: Core10.Buffer)
  -> Core10.DeviceSize
  -> IO ()
copyBuffer vw src dst size =
  oneshot vw $ \buf ->
    Core10.cmdCopyBuffer buf src dst [Core10.BufferCopy 0 0 size]
